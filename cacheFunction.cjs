function cacheFunction(cb) {
  try {
    const cache = {};
    return function (...args) {
      const key = JSON.stringify(args);

      if (key in cache) {
        return cache[key];
      }

      const res = cb(...args);
      cache[key] = res;

      return res;
    };
  } catch (error) {
    console.log(error);
  }
}

module.exports = cacheFunction;
