function limitFunctionCallCount(cb, n) {
  try {
    let invokeCount = 0;
    return function () {
      if (invokeCount + 1 <= n) {
        console.log("Callback called");
        invokeCount += 1;
        return cb();
      } else {
        console.log(`Limiting callback, reached limit of ${n} calls`);
        return null;
      }
    };
  } catch (error) {
    console.log(error);
  }
}

module.exports = limitFunctionCallCount;
