function counterFactory() {
  try {
    let counter = 0;
    return {
      increment: () => {
        counter = counter + 1;
        return counter;
      },
      decrement: () => {
        counter = counter - 1;
        return counter;
      },
    };
  } catch (error) {
    console.log(error)
  }
}

module.exports = counterFactory;
