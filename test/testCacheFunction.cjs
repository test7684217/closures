const cacheFunction = require("../cacheFunction.cjs");

let callCount = 0;

let test = cacheFunction((a, b) => {
  callCount += 1;
  return a + b;
});

test(2, 3);
test(23, 3);
test(2, 3);

console.log(`The callback function is invoked only ${callCount} times`);
