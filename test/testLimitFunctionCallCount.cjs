const limitFunctionCallCount = require( "../limitFunctionCallCount.cjs");

let x = limitFunctionCallCount(() => 3, 3);

console.log(x());
console.log(x());
console.log(x());
console.log(x());
